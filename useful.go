package gop

import "math"

// Distance returns the distance between two points
func Distance(x0 int, y0 int, x1 int, y1 int) float64 {
	return math.Sqrt(math.Pow(float64(x0)-float64(x1), 2) + math.Pow(float64(y0)-float64(y1), 2))
}

// MapRange maps an integer from a certain range to another range
func MapRange(number int, min float64, max float64, newMin float64, newMax float64) float64 {
	return newMin + ((newMax-newMin)/(max-min))*(float64(number)-min)
}

// MapFlRange maps a float from a certain range to another range
func MapFlRange(number float64, min float64, max float64, newMin float64, newMax float64) float64 {
	return newMin + ((newMax-newMin)/(max-min))*(float64(number)-min)
}

// DegreesToRadians converts degrees to radians
func DegreesToRadians(deg float64) float64 {
	return deg * (math.Pi / 180.0)
}

// RadiansToDegrees converts radians to degrees
func RadiansToDegrees(rad float64) float64 {
	return rad * (180.0 / math.Pi)
}

// LockInRange locks a number in a certain range
func LockInRange(number int, min int, max int) int {
	if number < min {
		return min
	} else if number > max {
		return max
	}
	return number
}

// RoundToInt rounds a float64 to an integer
func RoundToInt(number float64) int {
	return int(math.Round(number))
}
