package gop

// TransformationState saves a transformation state
type TransformationState struct {
	x        int
	y        int
	rotation float64
}

// ResetTransformations resets the transformation state and transformation stack
func (c *Canvas) ResetTransformations() {
	c.transformationStack = make([]TransformationState, 0)
	c.transformationState = TransformationState{
		x:        0,
		y:        0,
		rotation: 0.0,
	}
}

// ResetCurrentTransformations resets only the current transformation state
func (c *Canvas) ResetCurrentTransformations() {
	c.transformationState = TransformationState{
		x:        0,
		y:        0,
		rotation: 0.0,
	}
}

// GetPosition returns the current x,y position
func (c *Canvas) GetPosition() (int, int) {
	return c.transformationState.x, c.transformationState.y
}

// RotateDegrees rotates the current view by an angle, this will not affect previously drawn elements
func (c *Canvas) RotateDegrees(degrees float64) {
	c.transformationState.rotation += degrees
	c.capRotation()
}

// RotateRadians rotates the current view by radians, this will not affect previously drawn elements
func (c *Canvas) RotateRadians(radians float64) {
	c.transformationState.rotation += RadiansToDegrees(radians)
	c.capRotation()
}

func (c *Canvas) capRotation() {
	for c.transformationState.rotation >= 360 {
		c.transformationState.rotation -= 360
	}
	for c.transformationState.rotation <= -360 {
		c.transformationState.rotation += 360
	}
}

// MoveTo moves the current position to x,y ignoring rotation and movement
func (c *Canvas) MoveTo(x int, y int) {
	c.transformationState.x = x
	c.transformationState.y = y
}

// Translate same as MoveTo but relative to current position
func (c *Canvas) Translate(x int, y int) {
	_, _, x, y = applyTransformationToLine(0, 0, x, y, c.transformationState)
	c.transformationState.x = x
	c.transformationState.y = y
}

// Push saves the current transformation state
func (c *Canvas) Push() {
	c.transformationStack = append(c.transformationStack, c.transformationState)
}

// Pop restores the last saved transformation state
func (c *Canvas) Pop() {
	if len(c.transformationStack) > 0 {
		c.transformationState = c.transformationStack[len(c.transformationStack)-1]
		c.transformationStack = c.transformationStack[:len(c.transformationStack)-1]
	}
}
