package gop

import (
	"fmt"
	"math"
	"strconv"

	"gitlab.com/milan44/svgo"
)

// Line contains information about a line
type Line struct {
	x0 int
	y0 int
	x1 int
	y1 int
}

// Compare compares 2 lines
func (l Line) Compare(l1 Line) bool {
	return l.x0 == l1.x0 && l.y0 == l1.y0 && l.x1 == l1.x1 && l.y1 == l1.y1
}

// Line draws a line
func (c *Canvas) Line(x0 int, y0 int, x1 int, y1 int) {
	x0, y0, x1, y1 = applyTransformationToLine(x0, y0, x1, y1, c.transformationState)
	c.img.Line(x0, y0, x1, y1, c.colorState.strokeWeight, c.colorState.stroke.String())
}

// GetLine returns a line object including transformations
func (c *Canvas) GetLine(x0 int, y0 int, x1 int, y1 int) Line {
	x0, y0, x1, y1 = applyTransformationToLine(x0, y0, x1, y1, c.transformationState)
	return Line{
		x0: x0,
		y0: y0,
		x1: x1,
		y1: y1,
	}
}

// Rectangle draws a rectangle
func (c *Canvas) Rectangle(x0 int, y0 int, x1 int, y1 int) {
	width := x1 - x0
	height := y1 - y0

	ts := c.transformationState
	lines := [][]int{
		sl(applyTransformationToLine(x0, y0, x0, y1, ts)),
	}

	ll := lines[0]
	lines = append(lines, sl(applyTransformationToLine(ll[2], ll[3], ll[2]+width, ll[3], ts)))

	ll = lines[1]
	lines = append(lines, sl(applyTransformationToLine(ll[2], ll[3], ll[2], ll[3]-height, ts)))

	path := "M" + si(x0) + "," + si(y0)
	path += "L" + si(lines[0][2]) + "," + si(lines[0][3])
	path += "L" + si(lines[1][2]) + "," + si(lines[1][3])
	path += "L" + si(lines[2][2]) + "," + si(lines[2][3])
	path += "Z"

	obj := svgo.NewObject("p", path, svgo.Attributes(c.colorState.fill.String(), c.colorState.stroke.String(), c.colorState.strokeWeight))
	c.img.AddObject(obj)
}

// Ellipse draws an ellipse at x,y with the x radius rx and the y radius ry
func (c *Canvas) Ellipse(x int, y int, rx int, ry int) {
	_, _, x, y = applyTransformationToLine(x, c.Height, x, y, c.transformationState)

	attributes := svgo.Attributes(c.colorState.fill.String(), c.colorState.stroke.String(), c.colorState.strokeWeight)
	if rx == ry || math.Mod(c.transformationState.rotation, 360) != 0.0 {
		attributes += " transform=\"rotate(" + fmt.Sprintf("%g", c.transformationState.rotation) + ", " + strconv.Itoa(x) + ", " + strconv.Itoa(y) + ")\""
	}
	obj := svgo.Ellipse(x, y, rx, ry, attributes)

	c.img.AddObject(obj)
}

// Point draws a point with a given radius
func (c *Canvas) Point(x int, y int, r int) {
	x += c.transformationState.x
	y += c.transformationState.y
	c.img.Circle(x, y, r, c.colorState.fill.String(), c.colorState.stroke.String(), c.colorState.strokeWeight)
}

// Circle is an alias for Point
func (c *Canvas) Circle(x int, y int, r int) {
	c.Point(x, y, r)
}

// Background colors the background in a specific color
func (c *Canvas) Background(col Color) {
	c.img.Reset()
	c.img.Rect(0, 0, c.Width, c.Height, col.String(), "", 0)
}

// DrawPixel draws a "pixel" on the canvas
func (c *Canvas) DrawPixel(x int, y int, col Color) {
	c.img.Rect(x, y, 1, 1, col.String(), "", 0)
}
