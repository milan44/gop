package main

import (
	"math"

	"gitlab.com/milan44/gop"
)

func main() {
	c := gop.CreateCanvas(400, 400)
	angle := math.Pi / 4

	c.Background(gop.ColorBW(51, 255))
	c.Stroke(gop.ColorBW(255, 255))
	c.StrokeWeight(1)
	c.Translate(200, c.Height)

	branch(100.0, 10, angle, c)

	c.Export("sketch", false)
	c.ShowSketch()
}

func branch(len float64, depth int, angle float64, c *gop.Canvas) {
	c.Line(0, 0, 0, gop.RoundToInt(-len))
	c.Translate(0, gop.RoundToInt(-len))
	if depth > 0 {
		c.Push()
		c.RotateRadians(angle)
		branch(len*0.67, depth-1, angle, c)
		c.Pop()
		c.Push()
		c.RotateRadians(-angle)
		branch(len*0.67, depth-1, angle, c)
		c.Pop()
	}
}
