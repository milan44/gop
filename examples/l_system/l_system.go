package main

import (
	"gitlab.com/milan44/gop"
)

var angle float64
var axiom = "F"
var sentence = axiom
var leng float64 = 100

var rules = map[rune]string{
	'F': "FF+[+F-F-F]-[-F+F+F]",
}

func main() {
	c := gop.CreateCanvas(400, 400)
	angle = gop.DegreesToRadians(25)

	for i := 0; i < 4; i++ {
		generate(c)
	}

	c.Export("sketch", false)
	c.ShowSketch()
}

func generate(c *gop.Canvas) {
	leng *= 0.5
	var nextSentence = ""
	for i := 0; i < len(sentence); i++ {
		current := []rune(sentence)[i]
		found := false

		for key, val := range rules {
			if current == key {
				found = true
				nextSentence += val
				break
			}
		}
		if !found {
			nextSentence += string(current)
		}
	}
	sentence = nextSentence
	turtle(c)
}

func turtle(c *gop.Canvas) {
	c.Background(gop.ColorBW(51, 255))
	c.ResetTransformations()
	c.Translate(c.Width/2, c.Height)
	c.Stroke(gop.ColorBW(255, 100))
	for i := 0; i < len(sentence); i++ {
		current := []rune(sentence)[i]

		if current == 'F' {
			c.Line(0, 0, 0, -gop.RoundToInt(leng))
			c.Translate(0, -gop.RoundToInt(leng))
		} else if current == '+' {
			c.RotateRadians(angle)
		} else if current == '-' {
			c.RotateRadians(-angle)
		} else if current == '[' {
			c.Push()
		} else if current == ']' {
			c.Pop()
		}
	}
}
