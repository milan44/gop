package main

import (
	"math"

	"gitlab.com/milan44/gop"
)

func main() {
	c := gop.CreateCanvas(400, 400)
	i := gop.NewImage(c.Width, c.Height)

	maxiterations := 100.0

	for x := 0; x < c.Width; x++ {
		for y := 0; y < c.Height; y++ {
			a := gop.MapRange(x, 0, float64(c.Width), -2.5, 2.5)
			b := gop.MapRange(y, 0, float64(c.Height), -2.5, 2.5)

			ca, cb := a, b

			n := 0.0

			for n < maxiterations {
				aa := a*a - b*b
				bb := 2 * a * b
				a = aa + ca
				b = bb + cb
				if a*a+b*b > 16 {
					break
				}
				n++
			}

			bright := gop.MapFlRange(n, 0, maxiterations, 0, 1)
			bright = gop.MapFlRange(math.Sqrt(bright), 0, 1, 0, 255)

			if n == maxiterations {
				bright = 0
			}

			i.SetPixel(x, y, gop.ColorBW(gop.RoundToInt(bright), 255))
		}
	}

	c.RenderImage(i)

	c.Export("sketch", false)
	c.ShowSketch()
}
