package gop

import (
	"bytes"
	"encoding/base64"
	"errors"
	"image"
	"image/color"
	"image/jpeg"
	"image/png"
	"os"
	"strconv"
	"strings"

	"gitlab.com/milan44/svgo"
)

// Image is an image
type Image struct {
	image  *image.RGBA
	width  int
	height int
}

// NewImage creates a new Image
func NewImage(width int, height int) Image {
	topLeft := image.Point{0, 0}
	bottomRight := image.Point{width, height}

	return Image{
		image:  image.NewRGBA(image.Rectangle{topLeft, bottomRight}),
		width:  width,
		height: height,
	}
}

// NewImageFromFile tries to create an Image from an image file
func NewImageFromFile(file string) (*Image, error) {
	imagefile, err := os.Open(file)
	if err != nil {
		return nil, err
	}
	defer imagefile.Close()

	img, _, err := image.Decode(imagefile)
	if err != nil {
		return nil, err
	}

	if rgba, ok := img.(*image.RGBA); ok {
		b := rgba.Bounds()
		return &Image{
			image:  rgba,
			width:  b.Max.X,
			height: b.Max.Y,
		}, nil
	}
	return nil, errors.New("failed to convert image")
}

// SetPixel sets the color of a specific pixel
func (i *Image) SetPixel(x int, y int, c Color) {
	i.image.Set(x, y, color.RGBA{uint8(c.R), uint8(c.G), uint8(c.B), uint8(c.A)})
}

// GetPixel returns the color of a specific pixel
func (i *Image) GetPixel(x int, y int) Color {
	c := i.image.RGBAAt(x, y)
	return Color{
		R: int(c.R),
		G: int(c.G),
		B: int(c.B),
		A: int(c.A),
	}
}

// ToDataURI converts the image to a png data uri
func (i *Image) ToDataURI() string {
	buf := new(bytes.Buffer)
	_ = png.Encode(buf, i.image)
	bin := buf.Bytes()

	e64 := base64.StdEncoding

	maxEncLen := e64.EncodedLen(len(bin))
	encBuf := make([]byte, maxEncLen)

	e64.Encode(encBuf, bin)
	return "data:image/png;base64," + string(encBuf)
}

// Save saves the image to an image file
func (i *Image) Save(file string) error {
	out, err := os.Create(file)
	if err != nil {
		return err
	}
	defer out.Close()

	if strings.HasSuffix(file, ".png") {
		png.Encode(out, i.image)
	} else if strings.HasSuffix(file, ".jpg") || strings.HasSuffix(file, ".jpeg") {
		jpeg.Encode(out, i.image, nil)
	}
	return errors.New("unknown image type")
}

// RenderImage renders an Image to the canvas
func (c *Canvas) RenderImage(i Image) {
	uri := i.ToDataURI()
	obj := svgo.NewObject("image", "", "height=\""+strconv.Itoa(i.height)+"\" width=\""+strconv.Itoa(i.width)+"\" href=\""+uri+"\"")
	c.img.AddObject(obj)
}
