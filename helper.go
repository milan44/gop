package gop

import (
	"fmt"
	"math"
	"strconv"
)

func dec2hex(dec int) string {
	hex := fmt.Sprintf("%x", dec)
	for len(hex) < 2 {
		hex = "0" + hex
	}
	return hex
}

func hue2rgb(p, q, t float64) float64 {
	if t < 0.0 {
		t += 1.0
	}
	if t > 1.0 {
		t -= 1.0
	}
	if t < 1.0/6.0 {
		return p + (q-p)*6.0*t
	}
	if t < 1.0/2.0 {
		return q
	}
	if t < 2.0/3.0 {
		return p + (q-p)*(2.0/3.0-t)*6.0
	}
	return p
}

func applyTransformationToLine(x0 int, y0 int, x1 int, y1 int, ts TransformationState) (int, int, int, int) {
	if x0 >= 0 && y0 >= 0 && x1 >= 0 && y1 >= 0 {
		rad := DegreesToRadians(ts.rotation)

		x1 -= x0
		y1 -= y0

		x1F := float64(x1)
		y1F := float64(y1)

		x2 := (x1F * math.Cos(rad)) - (y1F * math.Sin(rad))
		y2 := (y1F * math.Cos(rad)) + (x1F * math.Sin(rad))

		x2 += float64(x0)
		y2 += float64(y0)

		return x0 + ts.x, y0 + ts.y, RoundToInt(x2) + ts.x, RoundToInt(y2) + ts.y
	}
	var nx0, ny0, ny1, nx1 int
	if x0 >= 0 && y0 >= 0 {
		_, _, nx0, ny0 = applyTransformationToLine(0, 0, x0, y0, ts)
	} else {
		tst := ts
		tst.rotation += 180
		_, _, nx0, ny0 = applyTransformationToLine(0, 0, abs(x0), abs(y0), tst)
	}
	if x1 >= 0 && y1 >= 0 {
		_, _, nx1, ny1 = applyTransformationToLine(0, 0, x1, y1, ts)
	} else {
		tst := ts
		tst.rotation += 180
		_, _, nx1, ny1 = applyTransformationToLine(0, 0, abs(x1), abs(y1), tst)
	}

	return nx0, ny0, nx1, ny1
}

func abs(i int) int {
	return int(math.Abs(float64(i)))
}

func sl(i ...int) []int {
	return i
}

func si(i int) string {
	return strconv.Itoa(i)
}
