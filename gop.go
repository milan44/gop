package gop

import (
	"errors"
	"os/exec"
	"path/filepath"
	"runtime"
	"strings"

	"gitlab.com/milan44/svgo"
)

// Canvas stores all drawings
type Canvas struct {
	img svgo.Image

	transformationState TransformationState
	transformationStack []TransformationState

	colorState ColorState
	colorStack []ColorState

	Width            int
	Height           int
	lastRenderedFile string
}

// CreateCanvas creates a canvas for drawing
func CreateCanvas(width int, height int) *Canvas {
	canvas := Canvas{
		img: svgo.Image{},
		transformationState: TransformationState{
			x:        0,
			y:        0,
			rotation: 0.0,
		},
		transformationStack: make([]TransformationState, 0),
		colorState: ColorState{
			strokeWeight: 1,
			stroke:       ColorBW(255, 255),
			fill:         ColorBW(0, 0),
		},
		colorStack: make([]ColorState, 0),
		Width:      width,
		Height:     height,
	}
	canvas.img.Init(width, height)

	return &canvas
}

// Stroke sets the stroke color
func (c *Canvas) Stroke(col Color) {
	c.colorState.stroke = col
}

// Fill sets the fill color
func (c *Canvas) Fill(col Color) {
	c.colorState.fill = col
}

// NoFill sets the fill color transparent
func (c *Canvas) NoFill() {
	c.colorState.fill = ColorBW(0, 0)
}

// NoStroke sets the stroke color transparent and the strokewidth to 0
func (c *Canvas) NoStroke() {
	c.colorState.stroke = ColorBW(0, 0)
	c.colorState.strokeWeight = 0
}

// StrokeWeight sets the width of stroke
func (c *Canvas) StrokeWeight(w int) {
	c.colorState.strokeWeight = w
}

// Export renders the canvas to a given file
func (c *Canvas) Export(filename string, dontDrawTwice bool) {
	if !strings.HasSuffix(filename, ".svg") {
		filename += ".svg"
	}

	c.img.RenderToFile(filename, dontDrawTwice)
	c.lastRenderedFile = filename
}

// Render renders the canvas to svg source
func (c *Canvas) Render(dontDrawTwice bool) string {
	return c.img.Render(dontDrawTwice)
}

// ShowSketch will show the last rendered canvas in your browser
func (c *Canvas) ShowSketch() error {
	var err error
	if c.lastRenderedFile != "" {
		url, _ := filepath.Abs(c.lastRenderedFile)
		url = "file:///" + url

		switch runtime.GOOS {
		case "linux":
			err = exec.Command("xdg-open", url).Start()
		case "windows":
			err = exec.Command("rundll32", "url.dll,FileProtocolHandler", url).Start()
		case "darwin":
			err = exec.Command("open", url).Start()
		default:
			err = errors.New("unsupported platform")
		}
	} else {
		err = errors.New("no sketch rendered")
	}
	return err
}
