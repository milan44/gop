package gop

// Color stores a color
type Color struct {
	R int
	G int
	B int
	A int
}

// ColorState holds the colors and stroke weight of a canvas
type ColorState struct {
	fill         Color
	stroke       Color
	strokeWeight int
}

func (c Color) String() string {
	if c.A == 0 {
		return "transparent"
	}
	return "#" + dec2hex(c.R) + dec2hex(c.G) + dec2hex(c.B) + dec2hex(c.A)
}

// Debug returns the Color values
func (c Color) Debug() string {
	return "Color{R:" + si(c.R) + ",G:" + si(c.G) + ",B:" + si(c.B) + ",A:" + si(c.A) + "}"
}

// ColorRGB creates a rgb color
func ColorRGB(r int, g int, b int, a int) Color {
	return Color{
		R: LockInRange(r, 0, 255),
		G: LockInRange(g, 0, 255),
		B: LockInRange(b, 0, 255),
		A: LockInRange(a, 0, 255),
	}
}

// ColorBW creates a black and white color
func ColorBW(w int, a int) Color {
	w = LockInRange(w, 0, 255)
	return Color{
		R: w,
		G: w,
		B: w,
		A: LockInRange(a, 0, 255),
	}
}

// ColorHSL creates an hsl color
func ColorHSL(hn int, sn int, ln int, a int) Color {
	color := Color{
		A: LockInRange(a, 0, 255),
	}

	h := float64(LockInRange(hn, 0, 360)) / 360
	s := float64(LockInRange(sn, 0, 100)) / 100
	l := float64(LockInRange(ln, 0, 100)) / 100

	var r, g, b float64
	if s == 0 {
		r = l
		g = l
		b = l
	} else {
		var q float64
		if l < 0.5 {
			q = l * (1 + s)
		} else {
			q = l + s - l*s
		}
		p := 2*l - q

		r = hue2rgb(p, q, h+1.0/3.0)
		g = hue2rgb(p, q, h)
		b = hue2rgb(p, q, h-1.0/3.0)
	}

	color.R = RoundToInt(r * 255)
	color.G = RoundToInt(g * 255)
	color.B = RoundToInt(b * 255)

	return color
}

// ColorPush saves the current color state
func (c *Canvas) ColorPush() {
	c.colorStack = append(c.colorStack, c.colorState)
}

// ColorPop restores the last saved color state
func (c *Canvas) ColorPop() {
	if len(c.colorStack) > 0 {
		c.colorState = c.colorStack[len(c.colorStack)-1]
		c.colorStack = c.colorStack[:len(c.colorStack)-1]
	}
}
