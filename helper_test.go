package gop

import (
	"testing"
)

func TestFunctions(t *testing.T) {
	_, _, gotX, gotY := applyTransformationToLine(10, 10, 10, 20, TransformationState{
		rotation: -90.0,
	})
	if gotX != 20 {
		t.Errorf("applyTransformationToLine: x = %d; want 20", gotX)
	}
	if gotY != 10 {
		t.Errorf("applyTransformationToLine: y = %d; want 10", gotY)
	}

	got := DegreesToRadians(90.0)
	if got != 1.5707963267948966 {
		t.Errorf("DegreesToRadians(90.0): %g; want 1.5707963267948966", got)
	}

	got = RadiansToDegrees(1.5707963267948966)
	if got != 90.0 {
		t.Errorf("DegreesToRadians(1.5707963267948966): %g; want 90.0", got)
	}

	gotI := RoundToInt(5.67)
	if gotI != 6 {
		t.Errorf("RoundToInt(5.67): %d; want 6", gotI)
	}

	gotI = RoundToInt(5.23)
	if gotI != 5 {
		t.Errorf("RoundToInt(5.23): %d; want 5", gotI)
	}

	got = Distance(2, 2, 12, 12)
	if got != 14.142135623730951 {
		t.Errorf("Distance: %g; want 14.142135623730951", got)
	}

	got = MapRange(50, 0, 100, 0, 1)
	if got != 0.5 {
		t.Errorf("MapRange: %g; want 0.5", got)
	}

	gotI = LockInRange(19, 0, 15)
	if gotI != 15 {
		t.Errorf("LockInRange(19, 0, 15): %d; want 15", gotI)
	}

	gotS := ColorHSL(0, 100, 50, 255)
	if gotS.String() != "#ff0000ff" {
		t.Errorf("ColorHSL(0, 100, 50, 255): " + gotS.String() + " (" + gotS.Debug() + "); want #ff0000ff")
	}
}

/*func TestTryingOutThingsUntilItWorks(t *testing.T) {
	c := CreateCanvas(400, 400)
	c.Background(ColorBW(51, 255))

	c.DrawPixel(20, 20, ColorBW(255, 255))
	c.DrawPixel(21, 20, ColorBW(255, 255))
	c.DrawPixel(22, 20, ColorBW(255, 255))

	c.Export("sketch")
	c.ShowSketch()
}
*/
